#Import data for covid-19 cases and AF bases
import pandas as pd
import requests
import io
import geopy.distance
import scipy as sp
import scipy.spatial
import numpy as np

#Data is under the creative commons licence.
#https://usafacts.org/visualizations/coronavirus-covid-19-spread-map/
COVIDByCounty_url = 'https://static.usafacts.org/public/data/covid-19/covid_confirmed_usafacts.csv'
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
response = requests.get(url=COVIDByCounty_url,headers=headers)

COVID_Data = pd.read_csv(io.StringIO(response.text))


County_Info = pd.read_csv('County_Info.csv')
#County_Info['Coords'] = zip(County_Info.Latitude,County_Info.Longitude)

AFB_Locs = pd.read_csv('AFB_Locs.csv')
#AFB_Locs['Coords']=zip(AFB_Locs.Lat,AFB_Locs.Long)
COVID_Data = pd.merge(County_Info,
                      COVID_Data,
                      right_on='countyFIPS',
                      left_on='FIPS',
                      how='inner')

Dist_Matrix = pd.DataFrame(
    np.zeros(len(COVID_Data) * len(AFB_Locs)).reshape(len(COVID_Data), len(AFB_Locs)),
    index=COVID_Data.index, columns=AFB_Locs.Base)

distances = pd.DataFrame(sp.spatial.distance.cdist(COVID_Data.loc[:,['Latitude','Longitude']],AFB_Locs.loc[:,['Lat','Long']],lambda u, v: geopy.distance.distance(u,v).miles))

Nearby_Counties = distances <100

Nearby_Pop = (Nearby_Counties.T * COVID_Data.loc[:,'Population']).T.sum(axis=0)
Current_Cases = (Nearby_Counties.T * COVID_Data.iloc[:,-1]).T.sum(axis=0)
Current_Rate = Current_Cases.divide(Nearby_Pop/1000)

